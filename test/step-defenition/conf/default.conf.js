
const nightwatchCucumber = require('nightwatch-cucumber');
const chromedriverPath = require('chromedriver').path;
const selenuimPath = require('selenium-server').path

nightwatchCucumber({
  cucumberArgs: [
    '--require', 'public/test/step-definitions',
    '--format', 'json:public/test/reports/cucumber.json',
    'public/test/features',
  ],
});

module.exports = {
  output_folder: 'public/test/reports',
  custom_assertions_path: '',
  page_objects_path: "public/test/page-objects",

  selenium: {
    start_process: true,
    server_path: selenuimPath,

    cli_args: {  
      'webdriver.chrome.driver': chromedriverPath,
    },
    args: [
      "start-fullscreen"
  ],
  },

  test_settings: {
    default: {
      launch_url : 'http://' + ('linkedin.com'),
      selenium_port: 4444,
      selenium_host: "127.0.0.1",
      screenshots: { "enabled": true, "path": "public/test/reports/screenshots"},
      desiredCapabilities: {
        browserName: 'chrome',
        "chromeOptions": {
          "args": [
              "window-size=1280,800"
          ]
      },
      javascriptEnabled : true,
      },
    },
    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
      },
    },
  },
};