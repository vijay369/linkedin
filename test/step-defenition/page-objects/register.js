module.exports = {


    url:  function () {
      return this.api.launchUrl;
  },
    
      elements: {
        loginform: '.login-form',
        registerform: '#regForm',
        mainheading : 'h2',
        subheading : 'h3',
        // logo:'.lazy-loaded',
        // loginemail:'#login-email',
        // loginpassword:'#login-password',
        // signinbutton:'.login submit-button',
        // forgotpassword:'.link-forgot-password',
        // firstname:'',
        // lastname:'',
        // formgroup:'.reg-form',

   
  
      },
      commands: [
        {
          isBodyDisplayed: function () {
            return this.waitForElementVisible('body',2000);
            
          },
          isDisplayedWithDelay: function (element) {
            return this.waitForElementPresent(element, 1000,function(){
              this.pause(200)
            });
          },
          
        }]
    }